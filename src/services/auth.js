// ref: https://blog.rocketseat.com.br/reactjs-autenticacao/

export const TOKEN_KEY = "@academictracker-Token";
export const isAuthenticated = () => localStorage.getItem(TOKEN_KEY) !== null;
export const getToken = () => localStorage.getItem(TOKEN_KEY);
export const login = token => {
  localStorage.setItem(TOKEN_KEY, token);
};
export const logout = () => {
  localStorage.removeItem(TOKEN_KEY);
};


export const getIDFromToken = token => {
  if (token) {
    try {
      //return JSON.parse(atob(token.split('.')[1]));
      return (JSON.parse(atob(token.split('.')[1]))).identity;
    } catch (error) {
      // ignore
    }
  }

  return null;
};