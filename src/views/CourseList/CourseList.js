import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { CourseToolbar, CourseTable } from './components';
import mockData from './data';
import { Grid } from '@material-ui/core';
import axios from 'axios'
import api from "../../services/api";

import { getIDFromToken, TOKEN_KEY } from "../../services/auth";



const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const CourseList = () => {
  const classes = useStyles();

  
  const [courses, setCourses] = useState([])


  const saveCourse = async (course) => {

   

     const token = localStorage.getItem(TOKEN_KEY)
     const idUser = getIDFromToken (token)

     try {
       const response = await api.post(`/api/v1/users/${idUser}/courses`, course );

       //login(response.data.access_token);
       console.log (response)
       //listCourses()
       setCourses([...courses,course])
     } catch (err) {
 
         console.log ("erro", err)
     }
 

  }
  

  const listCourses = () => {
    
    //http://127.0.0.1:8000/api/v1/users/8edd5b6e524511eab78dc0cb38773a6b/courses

    const token = localStorage.getItem(TOKEN_KEY)
    const idUser = getIDFromToken (token)

    //http://127.0.0.1:8000/api/v1/users/8edd5b6e524511eab78dc0cb38773a6b/courses
    api.get(`/api/v1/users/${idUser}/courses`)
      .then(function (response) {
        // handle success
        const coursesData = response.data
        console.log(coursesData)
        setCourses(coursesData)
        
      }).catch(function (error) {
        // handle error
        console.log(error);
      })
  }

  useEffect( () => {
    listCourses()
  }, [])

  return (
    <div className={classes.root}>


<Grid container spacing={6}>
  <Grid item xs={12}>
  <CourseToolbar saveCourse={saveCourse} />

  </Grid>

  <Grid item xs={12}>

  <div className={classes.content}>
        <CourseTable courses={courses} />
      </div>
    
    </Grid>
  
</Grid>

</div>
  );
};

export default CourseList;
