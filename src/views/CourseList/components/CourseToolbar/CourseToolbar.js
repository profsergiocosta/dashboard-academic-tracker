import React,{useState} from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { 
  Button,
  TextField,
  Grid,
  Select,
  FormControl,
  InputLabel,
  MenuItem

} from '@material-ui/core';

import { SearchInput } from 'components';
import { getIDFromToken, TOKEN_KEY } from "../../../../services/auth";
import api from "../../../../services/api"

const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1)
  }
}));

const UsersToolbar = props => {
  const { className, ...rest } = props;


  const [name, setName ] = useState('')
  const [URL, setURL ] = useState('')
  const [keywords, setKeywords ] = useState('')
  const [courseState, setCourseState ] = useState('')

   const submit = async event => {


     console.log ( {name,URL, keywords, courseState}  )
    
      try {
       const course =      {
          "title" : name,
          keywords,
          URL,
          courseState,
        };

        
        props.saveCourse(course)
      
      } catch (err) {
  
          console.log ("erro", err)
      }
  

   }

  const classes = useStyles();

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >


      
      <div className={classes.row}>

        <Grid container spacing={1}>




<Grid item xs={6}>
<TextField
          className={classes.searchInput}
          placeholder="Nome do curso"
          label="Nome:"
          fullWidth
          onChange ={e => setName (e.target.value) }
          value ={name}
        />
</Grid>

<Grid item xs={6}>
        <TextField
          className={classes.searchInput}
          placeholder="URL"
          label="URL:"
          fullWidth
          value = {URL}
          onChange ={e => setURL (e.target.value)}
        />
</Grid>


<Grid item xs={6}>
        <TextField
          className={classes.searchInput}
          placeholder="Palavras chaves"
          label="Palavras chaves:"
          fullWidth
          value = {keywords}
          onChange ={e => setKeywords (e.target.value)}
        />
</Grid>



<Grid item xs={4}>
<FormControl fullWidth>

  <InputLabel>Status:</InputLabel>
  <Select  value={courseState}  onChange ={e => setCourseState (e.target.value)}>
  
          <MenuItem value={"Quero cursar"}>Quero cursar</MenuItem>
          <MenuItem value={"Cursando"}>Cursando</MenuItem>
          <MenuItem value={"Concluído"}>Concluído</MenuItem>

  </Select>
</FormControl>


</Grid>


<Grid item xs={2}>
<Button
          color="secondary"
          variant="contained"
          fullWidth
          onClick={submit}
        >
          Adicionar
        </Button>
</Grid>

        </Grid>

      </div>



    </div>
  );
};

UsersToolbar.propTypes = {
  className: PropTypes.string
};

export default UsersToolbar;
